# IoTDevProject - Backend
> IoTDevProject는 IoT 시나리오를 생각하고 그대로 코딩없이 구현할 수 있도록 해주는 플랫폼이다.

## Project Stack

- Node.js
- Express
- TypeScript
- MongoDB
- Mongoose

## Run Local

### Prerequisites
- Node.js
- yarn
- mongoDB

### Installation

```bash
yarn install
```

### make .env file

```
NODE_PATH=src

DB_HOST=localhost
DB_USER=root
DB_PORT=27017
DB_NAME=IOTDev

SERVER_PORT=3000
TOKEN_SECRET=some-secret
REFRESH_SECRET=some-refresh-secret
TOKEN_LIFE=3600
REFRESH_LIFE=86400
```

### run Server

```bash
yarn start
```