import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import Controller from "./interface/controller.interface";
import {errorMiddleware} from "./middlewares/error.middleware";
import MongoDBConfigInterface from "./interface/mongoDBConfig.interface";
import SlowService from "./utils/SlowService";

class App {

  public app: express.Application;
  public port: number;
  public mongoHost: string;
  public mongoPort: string;
  public mongoName: string;

  constructor(controllers: Controller[], port:number, DBConfig: MongoDBConfigInterface) {
    this.app = express();
    this.port = port;
    this.mongoHost = DBConfig.HOST;
    this.mongoPort = DBConfig.PORT;
    this.mongoName = DBConfig.NAME;
    this.initializeMiddleware();
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
    this.mongoSetup();
  }

  private initializeMiddleware() {
    SlowService.getKey();
    this.app.use(this.loggerMiddleware);
    this.app.use(cors());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(express.static('public'));
  }

  private initializeControllers(controllers: any) {
    controllers.forEach((controller: any) => {
      this.app.use('/', controller.router);
    });
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private mongoSetup(): void{
    mongoose.connect(`mongodb://${this.mongoHost}:${this.mongoPort}/${this.mongoName}`, {
          useNewUrlParser: true,
          useCreateIndex: true,
    });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', () => console.log('mongoDB connected'));
  }

  private loggerMiddleware(request: express.Request, response: express.Response, next: express.NextFunction) {
    console.log(`${request.method} ${request.path}`);
    next();
  }

  public listen() {
    this.app.listen(this.port, () => {
      console.log(`App listening on the port ${this.port}`);
    });
  }
}

export default App