import * as mongoose from "mongoose";
import Scenario from "../interface/scenario.interface";

const Schema = mongoose.Schema;

const scenarioSchema = new Schema({
  account: {
    ref: 'User',
    type: mongoose.Schema.Types.ObjectId,
    default: null
  },
  name: {
    type: String,
    min: 2,
    max: 15,
    required: true,
    unique: true
  },
  functionList: [{
    functionName: String,
    params: Object
  }]
});

const ScenarioModel = mongoose.model<Scenario & mongoose.Document>('Scenario', scenarioSchema);

export default ScenarioModel