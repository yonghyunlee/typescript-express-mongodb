import * as mongoose from "mongoose";
import User from "../interface/user.interface";

const Schema = mongoose.Schema;

const userSchema = new Schema({
  username: {
    type: String,
    min: 2,
    max: 15,
    lowercase: true,
    required: true,
    unique: true
  },
  password: {
    type: String,
    min: 2,
    max: 15,
    required: true
  },
  refreshToken: {
    type: String,
    default: null
  }
});

const UserModel = mongoose.model<User & mongoose.Document>('User', userSchema);

export default UserModel