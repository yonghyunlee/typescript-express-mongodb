import HttpException from './HttpException';

class WrongAuthenticationGrantException extends HttpException {
  constructor() {
    super(401, 'Wrong authentication grant_type');
  }
}

export default WrongAuthenticationGrantException;