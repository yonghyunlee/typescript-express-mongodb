import HttpException from './HttpException';

class FunctionAlreadyExistsException extends HttpException {
  constructor(name: string) {
    super(400, `Function ${name} already exists`);
  }
}

export default FunctionAlreadyExistsException;