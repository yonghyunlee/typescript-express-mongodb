import HttpException from './HttpException';

class ScenarioAlreadyExistsException extends HttpException {
  constructor(name: string) {
    super(400, `scenario ${name} already exists`);
  }
}

export default ScenarioAlreadyExistsException;