export const FunctionList = {
  TIME_LIGHT: "time-light",
  TIME_FAN: "time-fan",
};

export const PlayingFunctionList: Map<string, Array<string>> = new Map();