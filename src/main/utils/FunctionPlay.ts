import {scheduleList} from "./schedule";
import {Job, scheduleJob} from "node-schedule";
import SlowService from "./SlowService";
import PlayFunctionInterface from "../interface/playFunction.interface";
import {controlHKDeviceByApi} from "./HKUtil";

export const timeLight = (scenarioId: string, func: PlayFunctionInterface) => {
  let schedules: Array<Job> = [];

  const time = func.params.time.split(':');

  const job = scheduleJob({
    hour: parseInt(time[0]),
    minute: parseInt(time[1]),
  }, async () => {
    try {
      if (func.params.command === 'on') {
        await controlHKDeviceByApi(process.env.HK_SMARTPLUG_LIGHT_MAC, process.env.HK_SMARTPLUG_LIGHT_DEVICE_ID, 'on');
      } else {
        await controlHKDeviceByApi(process.env.HK_SMARTPLUG_LIGHT_MAC, process.env.HK_SMARTPLUG_LIGHT_DEVICE_ID, 'off');
      }
    } catch (e) {
      throw e;
    }
  });

  schedules.push(job);
  scheduleList.set(scenarioId, schedules);

  return true;
};

export const timeFan = (scenarioId: string, func: PlayFunctionInterface) => {
  let schedules: Array<Job> = [];

  const time = func.params.time.split(':');

  const job = scheduleJob({
    hour: parseInt(time[0]),
    minute: parseInt(time[1]),
  }, async () => {
    try {
      if (func.params.command === 'on') {
        await controlHKDeviceByApi(process.env.HK_SMARTPLUG_FAN_MAC, process.env.HK_SMARTPLUG_FAN_DEVICE_ID, 'on');
      } else {
        await controlHKDeviceByApi(process.env.HK_SMARTPLUG_FAN_MAC, process.env.HK_SMARTPLUG_FAN_DEVICE_ID, 'off');
      }
    } catch (e) {
      throw e;
    }
  });

  schedules.push(job);
  scheduleList.set(scenarioId, schedules);

  return true;
};