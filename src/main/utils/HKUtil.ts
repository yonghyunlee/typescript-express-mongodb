import Axios from "axios";
import 'dotenv';

export const controlHKDeviceByApi = async (HKMac: string, HKDeviceId: string, cmd: string) => {
    try {
        const options = {
            method: 'POST',
            url: 'https://open.hknetworks.kr/smarthome/controlDevice',
            headers: {
                'cache-control': 'no-cache',
                'Content-Type': 'application/json'
            },
            data: {
                userid: process.env.HK_EMAIL,
                password: process.env.HK_PASSWORD,
                mac: HKMac,
                deviceId: HKDeviceId,
                command: { type: `turn${cmd}` }
            }
        };

        const response = await Axios(options);
        console.log(`${process.env.HK_SMARTPLUG_MAC} ${cmd} result: ${JSON.stringify(response.data)}`);

    } catch (e) {
        console.error(e);
    }
};