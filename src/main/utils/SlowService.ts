import axios from 'axios';

class SlowService {
  accessToken: string;
  accountId: number;
  apiKey: string;

  constructor() {}

  public getKey = async () => {
    const url = "https://auth.techandslow.com/api/v1/app/accounts/signin";
    const options = {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      data: {
        username: process.env.SLOW_ID,
        password: process.env.SLOW_PASSWORD
      },
      url,
    };

    try {
      const data = await axios(options);

      this.accessToken = data.headers['x-access-token'];
      this.accountId = data.data.accountId;
      this.apiKey = data.data.apiKey;

      return true;
    } catch (e) {
      throw e;
    }
  };

  public requestDigital = async (command: boolean) => {
    const url = "https://auth.techandslow.com/api/v1/app/accounts/4/hubs/455/devices/327/commands/digital";
    let cmd = "0001xxxx";

    if (!command) {
      cmd = "0000xxxx";
    }

    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': this.accessToken,
        'api-key': this.apiKey
      },
      data: {
        "secureKey": "ofdy",
        "syncTimestamp": new Date().getTime(),
        "digital": cmd
      },
      url
    };

    try {
      await axios(options);

      return true;
    } catch (e) {
      throw e;
    }
  }
}

export default new SlowService();