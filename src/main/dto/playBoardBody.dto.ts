import {IsArray, IsMongoId} from 'class-validator'
import PlayFunctionInterface from "../interface/playFunction.interface";

class PlayBoardBodyDto {

  @IsMongoId()
  public scenarioId: string;

  @IsArray()
  public functionList: Array<PlayFunctionInterface>;
}

export default PlayBoardBodyDto;