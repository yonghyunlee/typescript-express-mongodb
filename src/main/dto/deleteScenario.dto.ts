import { IsString } from 'class-validator'

class DeleteScenarioDto {
  @IsString()
  public _id: string;
}

export default DeleteScenarioDto;