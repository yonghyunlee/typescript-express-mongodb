import { IsString } from 'class-validator';

class getTokenBody {
  @IsString()
  public grant_type: string;

  @IsString()
  public refresh_token: string;
}

export default getTokenBody;