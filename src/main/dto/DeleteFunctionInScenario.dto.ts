import {IsMongoId} from 'class-validator'

class DeleteFunctionInScenarioDto {
  @IsMongoId()
  public scenarioId: string;

  @IsMongoId()
  public functionId: string;
}

export default DeleteFunctionInScenarioDto;