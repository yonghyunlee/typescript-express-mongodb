import { IsString } from 'class-validator'

class CreateScenarioDto {
  @IsString()
  public name: string;
}

export default CreateScenarioDto;