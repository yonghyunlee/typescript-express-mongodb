import { IsString } from 'class-validator'

class UpdateScenarioDto {
  @IsString()
  public _id: string;

  @IsString()
  public newName: string;
}

export default UpdateScenarioDto;