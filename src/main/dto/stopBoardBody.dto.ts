import {IsMongoId} from 'class-validator'

class StopBoardBodyDto {

  @IsMongoId()
  public scenarioId: string;
}

export default StopBoardBodyDto;