import {IsArray, IsMongoId, IsString} from 'class-validator'
import PlayFunctionInterface from "../interface/playFunction.interface";

class AddFunctionInScenarioDto {
  @IsMongoId()
  public scenarioId: string;

  @IsString()
  public functionName: string;

  public params: PlayFunctionInterface;
}

export default AddFunctionInScenarioDto;