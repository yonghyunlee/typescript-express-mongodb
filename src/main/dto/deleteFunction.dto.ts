import { IsString } from 'class-validator'

class DeleteFunctionDto {
  @IsString()
  public name: string;
}

export default DeleteFunctionDto;