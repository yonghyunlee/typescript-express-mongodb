import {IsMongoId} from 'class-validator'
import PlayFunctionInterface from "../interface/playFunction.interface";

class ModifyFunctionInScenarioDto {
  @IsMongoId()
  public scenarioId: string;

  @IsMongoId()
  public functionId: string;

  public params: PlayFunctionInterface;
}

export default ModifyFunctionInScenarioDto;
