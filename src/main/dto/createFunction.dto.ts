import {ArrayNotEmpty, IsString} from 'class-validator'

class CreateFunctionDto {
  @IsString()
  public name: string;

  @ArrayNotEmpty()
  params: Array<object>;
}

export default CreateFunctionDto;