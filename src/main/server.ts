import App from './app'
import AuthenticationController from "./controllers/authController";
import 'dotenv/config'
import MongoDBConfigInterface from "./interface/mongoDBConfig.interface";
import ScenarioController from "./controllers/scenarioController";
import BoardController from "./controllers/boardController";

const DBConfig: MongoDBConfigInterface = {
  HOST: process.env.DB_HOST,
  PORT: process.env.DB_PORT,
  NAME: process.env.DB_NAME
};

const app = new App([
    new AuthenticationController(),
    new ScenarioController(),
    new BoardController()
  ],
  parseInt(process.env.SERVER_PORT, 10),
  DBConfig
);

app.listen();