import Controller from "../interface/controller.interface";
import * as express from "express";
import ScenarioModel from "../models/scenarioSchema";
import { authMiddleware } from "../middlewares/auth.middleware";
import RequestWithUser from "../interface/requestWithUser.interface";
import CreateScenarioDto from "../dto/createScenario.dto";
import ScenarioAlreadyExistsException from "../exceptions/ScenarioAlreadyExistsException";
import UpdateScenarioDto from "../dto/updateScenario.dto";
import DeleteScenarioDto from "../dto/deleteScenario.dto";
import BadRequestException from "../exceptions/BadRequestException";

class ScenarioController implements Controller {
  public path = '/scenario';
  public router = express.Router();
  private scenarioModel = ScenarioModel;

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.route(this.path).all(authMiddleware)
      .get(this.getScenario)
      .post(this.addScenario)
      .put(this.updateScenario)
      .delete(this.deleteScenario);
  }

  private getScenario = async (req: RequestWithUser, res: express.Response, next: express.NextFunction) => {
    try {
      const scenarioList = await this.scenarioModel.find({account: req.user._id}, {account: 0, Function: 0,__v: 0});

      // const sendData = JSON.parse(JSON.stringify(scenarioList).replace(`\"_id\":`, `\"id\":`));

      res.json(scenarioList);

    } catch (e) {
      console.error(e);
      res.sendStatus(500);
    }
  };

  private addScenario = async (req: RequestWithUser, res: express.Response, next: express.NextFunction) => {

    console.log(req.headers);
    const scenarioData: CreateScenarioDto = req.body;

    console.log(scenarioData);

    if (await this.scenarioModel.findOne({ account: req.user._id, name: scenarioData.name })) {
      next(new ScenarioAlreadyExistsException(scenarioData.name));
    } else {
      await this.scenarioModel.create({
        ...scenarioData,
        account: req.user._id
      });
    }

    res.sendStatus(200)
  };

  private updateScenario = async (req: RequestWithUser, res: express.Response, next: express.NextFunction) => {

    const scenarioData: UpdateScenarioDto = req.body;

    const isScenarioExist = await this.scenarioModel.find({ account: req.user._id, name: scenarioData.newName });

    if (isScenarioExist) {
      next(new ScenarioAlreadyExistsException(scenarioData.newName));
    }

    try {

      await this.scenarioModel.findOneAndUpdate({ account: req.user._id, _id: scenarioData._id }, {
        $set: {
          name: scenarioData.newName
        }
      });

      res.sendStatus(200)

    } catch (e) {
      console.error(e);

      res.sendStatus(500);
    }
  };

  private deleteScenario = async (req: RequestWithUser, res: express.Response, next: express.NextFunction) => {

    const scenarioData: DeleteScenarioDto = req.body;

    try {
      await this.scenarioModel.deleteOne({ account: req.user._id, _id: scenarioData._id });

      res.sendStatus(200);
    } catch (e) {
      next(new BadRequestException())
    }
  };
}

export default ScenarioController;