import UserModel from '../models/userSchema';
import Controller from "../interface/controller.interface";
import * as express from "express"
import {validationMiddleware} from "../middlewares/validation.middleware";
import CreateUserDto from "../dto/createUser.dto";
import LogInDto from "../dto/logIn.dto";
import UsernameAlreadyExistsException from "../exceptions/UsernameAlreadyExistsException";
import * as bcrypt from 'bcrypt'
import TokenData from "../interface/tokenData.interface";
import User from "../interface/user.interface";
import * as jwt from 'jsonwebtoken'
import DataStoredInToken from "../interface/dataStoredInToken.interface";
import WrongCredentialsException from "../exceptions/WrongCredentialsException";
import RequestWithUser from "../interface/requestWithUser.interface";
import WrongAuthenticationTokenException from "../exceptions/WrongAuthenticationTokenException";
import AuthenticationTokenMissingException from "../exceptions/AuthenticationTokenMissingException";
import WrongAuthenticationGrantException from "../exceptions/WrongAuthenticationGrantException";
import getTokenBody from "../dto/getTokenBody.dto";
import {authMiddleware} from "../middlewares/auth.middleware";


class AuthenticationController implements Controller {
  public path = '/auth';
  public router = express.Router();
  private user = UserModel;

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/register`, validationMiddleware(CreateUserDto), this.registration);
    this.router.post(`${this.path}/login`, validationMiddleware(LogInDto), this.logIn);
    this.router.post(`${this.path}/token`, validationMiddleware(getTokenBody),this.getAccessToken);
    this.router.get(`${this.path}/check`, authMiddleware, this.checkToken);
  }

  private registration = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const userData: CreateUserDto = req.body;

    if (await this.user.findOne({username: userData.username})) {
      next(new UsernameAlreadyExistsException(userData.username));
    } else {
      const hashedPassword = await bcrypt.hash(userData.password, 10);
      await this.user.create({...userData, password: hashedPassword});
      res.sendStatus(200)
    }
  };

  private logIn = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const logInData: LogInDto = req.body;

    if (logInData.grant_type !== "password") {
      next(new WrongAuthenticationGrantException)
    }

    const user = await this.user.findOne({username: logInData.username});


    if (user) {
      const isPasswordMatching = await bcrypt.compare(logInData.password, user.password);
      if (isPasswordMatching) {
        const accessTokenData = AuthenticationController.createToken(user);
        const refreshTokenData = AuthenticationController.createRefreshToken(user);

        await UserModel.updateOne({username: user.username}, {
          $set: {
            refreshToken: refreshTokenData.token
          }
        });

        res.json({
          tokenType: "bearer",
          accessToken: accessTokenData.token,
          expiresIn: accessTokenData.expiresIn,
          refreshToken: refreshTokenData.token
        });
      } else {
        next(new WrongCredentialsException());
      }
    } else {
      next(new WrongCredentialsException());
    }
  };

  private getAccessToken = async (req: RequestWithUser, res: express.Response, next: express.NextFunction) => {

    const reqRefreshTokenBody: getTokenBody = req.body;

    if (reqRefreshTokenBody.grant_type !== "refresh_token") {
      next(new WrongAuthenticationGrantException())
    }

    const refreshToken = reqRefreshTokenBody.refresh_token;

    if (refreshToken) {
      const secret = process.env.REFRESH_SECRET;

      try {

        const verificationResponse = jwt.verify(refreshToken, secret) as DataStoredInToken;

        console.log(verificationResponse);

        const user = await UserModel.findOne({refreshToken});

        console.log(user);
        if (user) {
          const accessTokenData = AuthenticationController.createToken(user);
          const refreshTokenData = AuthenticationController.createRefreshToken(user);

          await UserModel.updateOne({username: user.username}, {
            $set: {
              refreshToken: refreshTokenData.token
            }
          });

          res.json({
            tokenType: "bearer",
            accessToken: accessTokenData.token,
            expiresIn: accessTokenData.expiresIn,
            refreshToken: refreshTokenData.token
          });
        } else {
          next(new WrongAuthenticationTokenException());
        }
      } catch (error) {
        console.error(error);
        next(new WrongAuthenticationTokenException());
      }
    } else {
      next(new AuthenticationTokenMissingException());
    }
  };

  private checkToken(req: RequestWithUser, res: express.Response, next: express.NextFunction) {
    res.sendStatus(200);
  }

  private static createToken(user: User): TokenData {
    const expiresIn = parseInt(process.env.TOKEN_LIFE, 10);
    const secret = process.env.TOKEN_SECRET;
    const dataStoredInToken: DataStoredInToken = {
      _id: user._id,
    };
    return {
      expiresIn,
      token: jwt.sign(dataStoredInToken, secret, {expiresIn}),
    };
  }

  private static createRefreshToken(user: User): TokenData {
    const expiresIn = parseInt(process.env.REFRESH_LIFE, 10);
    const secret = process.env.REFRESH_SECRET;
    const dataStoredInToken: DataStoredInToken = {
      _id: user._id,
    };
    return {
      expiresIn,
      token: jwt.sign(dataStoredInToken, secret, {expiresIn}),
    };
  }
}

export default AuthenticationController;