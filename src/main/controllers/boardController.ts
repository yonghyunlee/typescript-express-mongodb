import Controller from "../interface/controller.interface";
import * as express from "express";
import ScenarioModel from "../models/scenarioSchema";
import { authMiddleware } from "../middlewares/auth.middleware";
import RequestWithUser from "../interface/requestWithUser.interface";
import BadRequestException from "../exceptions/BadRequestException";
import {validationMiddleware} from "../middlewares/validation.middleware";
import AddFunctionInScenarioDto from "../dto/addFunctionInScenario.dto";
import DeleteFunctionInScenarioDto from "../dto/DeleteFunctionInScenario.dto";
import PlayBoardBodyDto from "../dto/playBoardBody.dto";
import {timeFan, timeLight} from "../utils/FunctionPlay";
import PlayFunctionInterface from "../interface/playFunction.interface";
import {FunctionList, PlayingFunctionList} from "../utils/FunctionList";
import {scheduleList} from "../utils/schedule";
import {Job} from "node-schedule";
import StopBoardBodyDto from "../dto/stopBoardBody.dto";
import ModifyFunctionInScenarioDto from "../dto/modifyFunctionInScenario.dto";

class BoardController implements Controller {
  public path = '/board';
  public router = express.Router();
  private scenarioModel = ScenarioModel;

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.use(authMiddleware);
    this.router.get(`${this.path}/:id`, this.getOneScenario);
    this.router.post(`${this.path}`, validationMiddleware(AddFunctionInScenarioDto), this.addFunctionInScenario);
    this.router.put(`${this.path}`, validationMiddleware(ModifyFunctionInScenarioDto), this.modifyFunctionInScenario);
    this.router.delete(`${this.path}`, validationMiddleware(DeleteFunctionInScenarioDto), this.deleteFunctionInScenario);
    this.router.post(`${this.path}/play`, validationMiddleware(PlayBoardBodyDto), this.playBoard);
    this.router.delete(`${this.path}/stop`, validationMiddleware(StopBoardBodyDto), this.stopBoard);
  }

  private getOneScenario = async (req: RequestWithUser, res: express.Response) => {
    try {
      const scenario = await this.scenarioModel.findOne(
        {account: req.user._id, _id: req.params.id},
        {account: 0, __v: 0}
        );

      res.json(scenario);
    } catch (e) {
      console.error(e);
      res.sendStatus(500);
    }
  };

  private addFunctionInScenario = async (req: RequestWithUser, res: express.Response) => {

    const reqBody: AddFunctionInScenarioDto = req.body;

    try {
      await this.scenarioModel.findByIdAndUpdate(reqBody.scenarioId, {
        $push: {
          functionList: {
            functionName: reqBody.functionName,
            params: reqBody.params
          }
        }
      });
    } catch (e) {
      console.error(e);
      res.sendStatus(500);
    }

    res.sendStatus(200)
  };

  private modifyFunctionInScenario = async (req: RequestWithUser, res: express.Response) => {

    const reqBody: ModifyFunctionInScenarioDto = req.body;
    console.log(reqBody);

    try {
      await this.scenarioModel.findOneAndUpdate({
        _id: reqBody.scenarioId,
        functionList:  {
          $elemMatch: {
            _id: reqBody.functionId
          }
        }
      }, {
        $set: {
          'functionList.$.params': reqBody.params
        }
      });
    } catch (e) {
      console.error(e);
      res.sendStatus(500);
    }

    res.sendStatus(200)
  };

  private deleteFunctionInScenario = async (req: RequestWithUser, res: express.Response, next: express.NextFunction) => {

    const reqBody: DeleteFunctionInScenarioDto = req.body;

    try {
      await this.scenarioModel.findByIdAndUpdate(reqBody.scenarioId, {
        $pull: {
          functionList: {
            _id: reqBody.functionId
          }
        }
      });

      res.sendStatus(200);
    } catch (e) {
      next(new BadRequestException())
    }
  };

  private playBoard = async (req: RequestWithUser, res: express.Response, next: express.NextFunction) => {

    const reqBody: PlayBoardBodyDto = req.body;
    
    if (!reqBody.functionList) {
      next(new BadRequestException());
    }

    const searchedSchedule: Array<Job> = scheduleList.get(reqBody.scenarioId);

    if (searchedSchedule) {
      searchedSchedule.forEach((job: Job) => job.cancel())
    }

    let list: Array<string> = [];
    reqBody.functionList.forEach((func: PlayFunctionInterface) => {
      try {
        switch (func.functionName) {
          case FunctionList.TIME_LIGHT:
            timeLight(reqBody.scenarioId, func);
            break;
          case FunctionList.TIME_FAN:
            timeFan(reqBody.scenarioId, func);
            break;
          default:
            break;
        }
        list.push(func.functionName);
      } catch (e) {
        next(new BadRequestException());
      }
    });
    PlayingFunctionList.set(req.user.username, list);

    res.sendStatus(200);
  };

  private stopBoard = async (req: RequestWithUser, res: express.Response, next: express.NextFunction) => {

    const reqBody: { scenarioId: string } = req.body;

    if (!reqBody.scenarioId) {
      next(new BadRequestException());
    }

    try {
      const playingFunctionList = PlayingFunctionList.get(req.user.username);
      if (!playingFunctionList) {
        return res.status(200).json({
          message: 'Do nothing',
          status: 0
        })
      }

      playingFunctionList.forEach((value) => {
        if (value === FunctionList.TIME_LIGHT || value === FunctionList.TIME_FAN) {
          scheduleList.get(reqBody.scenarioId).forEach((job: Job) => {
            job.cancel();
          });
        }
      });
      res.status(200).json({
        message: 'success',
        status: 1
      });
    } catch (e) {
      next(new BadRequestException());
    }
  };
}

export default BoardController;