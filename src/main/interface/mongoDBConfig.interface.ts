import 'dotenv/config'

interface MongoDBConfigInterface {
  HOST: string
  PORT: string
  NAME: string
}

export default MongoDBConfigInterface;