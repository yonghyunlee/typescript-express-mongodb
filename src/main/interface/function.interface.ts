interface Function {
  _id: string;
  name: string;
  params: Array<object>;
}

export default Function;