interface Scenario {
  _id: string;
  name: string;
  account: string;
  Function: Array<object>;
}

export default Scenario;