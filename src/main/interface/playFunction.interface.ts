interface PlayFunctionInterface {
  functionName: string;
  params: ParamsInterface;
}

export interface ParamsInterface {
  time: string,
  command: string,
  device: string
}

export default PlayFunctionInterface;